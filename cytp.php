<?php
/*
Plugin Name: WP Video Ace Basic

Plugin URI:

Description: Take full control of your videos with powerful engagement effects and custom playback options.

Author: Dan Green

Version: 1.1.2

Author URI: 
*/

// deactivate self if pro is activated
function cytp_detect_pro_version() {
	if ( is_plugin_active( 'WPVideoAcePro/cytp.php' ) ) {
		deactivate_plugins( plugin_basename( __FILE__ ) );
		echo "Pro plugin exists";
		die(1);
	}
}
register_activation_hook( __FILE__, 'cytp_detect_pro_version' );

require_once 'plugin_update_check.php';
$MyUpdateChecker = new PluginUpdateChecker_2_0 (
   'https://kernl.us/api/v1/updates/5752dd50b321d41a1f48e0c6/',
   __FILE__,
   'cytp',
   1
);

// define absolute path
define( 'CYTP_PLUGIN_PATH', ABSPATH . 'wp-content/plugins/WPVideoAce' );

// define absolute url
define( 'CYTP_PLUGIN_URL', plugins_url( '', __FILE__ ) );

require_once( CYTP_PLUGIN_PATH . '/vendor/meta-box/meta-box.php' );
if ( ! defined( 'MBC_INC_DIR' ) ) {
	require_once( CYTP_PLUGIN_PATH . '/vendor/meta-box-conditional-logic/meta-box-conditional-logic.php' );
}

// --- Text Domain ---
add_action( 'after_setup_theme', 'cytp_load_plugin_cytp_plugin' );
function cytp_load_plugin_cytp_plugin() {
	load_plugin_textdomain( 'cytp_plugin', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
}
// --- End of Text Domain --- 

// --- Styles ---
add_action( 'wp_enqueue_scripts', 'cytp_custom_assets' );
add_action( 'wp_footer', 'cytp_custom_scripts' );
add_action( 'admin_enqueue_scripts', 'cytp_admin_assets' );

function cytp_custom_assets() {
	wp_enqueue_style( 'wp-mediaelement' );
	wp_enqueue_style( 'cytp-style', CYTP_PLUGIN_URL . '/styles/style.css' );
}

function cytp_custom_scripts() {
    wp_enqueue_script('wp-mediaelement');
    wp_enqueue_script( 'cytp-script', CYTP_PLUGIN_URL . '/scripts/script.js' );
}

function cytp_admin_assets() {
	wp_enqueue_style( 'cytp-admin-style', CYTPRO_PLUGIN_URL . '/styles/style.css' );
}
// --- End of Styles ---

// --- Custom Post Type ---
function cytp_register_post_type_cytp() {
	register_post_type ( 'cytp_video', array (
		'labels' => array (
			'name' 					=> esc_html__( 'Video Players', 'cytp_plugin' ),
			'all_items' 			=> esc_html__( 'Video Players', 'cytp_plugin' ),
			'singular_name' 		=> esc_html__( 'Video', 'cytp_plugin' ),
			'add_new_item'			=> __( 'Add New Video Player', 'cytp_plugin' ),
			'edit_item'				=> __( 'Edit Video Player', 'cytp_plugin' ),
			'new_item'				=> __( 'New Video Player', 'cytp_plugin' ),
			'view_item'				=> __( 'View Video Player', 'cytp_plugin' ),
			'search_items'			=> __( 'Search Video Player', 'cytp_plugin' ),
			'not_found'				=> __( 'No Video Player found', 'cytp_plugin' ),
			'not_found_in_trash'	=> __( 'No Video Player found in trash', 'cytp_plugin' ),
			'menu_name'				=> __( 'WP Video Ace', 'cytp_plugin' ),
			'parent_item_colon'		=> '',
		),
		'public' => true,
		'has_archive' => false,
		'exclude_from_search' => true,
		'rewrite' => array (
			'slug' => 'videos',
		),
		'supports' => array (
			'title',
			// 'custom-fields'
			// 'editor',
			// 'thumbnail',
		),
		'can_export' => true,
		'menu_icon'	=> 'dashicons-format-gallery'
	) );
}

add_action( 'init', 'cytp_register_post_type_cytp' );
// --- End of Custom Post Type ---

// Add Shortcode Column to Admin Section
add_filter('manage_cytp_video_posts_columns', 'cytp_add_column', 10);
add_action('manage_cytp_video_posts_custom_column', 'cytp_add_column_content', 10, 2);

// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function cytp_add_column($defaults) {
	$defaults['shortcode'] = 'Shortcode';
	return $defaults;
}
function cytp_add_column_content($column_name, $post_ID) {
	if ($column_name === 'shortcode') {
		echo "[cytp_player name=\"" . get_the_title( $post_ID ) . "\"][/cytp_player]";
	}

	return true;
}

// --- Shortcode ---

function cytp_player_handler( $atts ) {
	$a = shortcode_atts( array(
			'name' => '',
	), $atts );
	$postname = $a['name'];

	$video = get_page_by_title( $postname, OBJECT, 'cytp_video');

	$containerId = uniqid();
	$output = "";

	if ( empty( $video ) ) {
		$output = "<div class='cytp-error'>No such video</div>";
	}
	else {
		$video_type		= get_post_meta( $video->ID, 'cytp_video_type', true ); // type_media | type_external | type_youtube

		if ( $video_type == 'type_media') {
			$attach_id		= get_post_meta( $video->ID, 'cytp_video_media', true );
			$video_url		= wp_get_attachment_url( $attach_id );
		} else if ( $video_type == 'type_external') {
			$video_url 		= get_post_meta( $video->ID, 'cytp_video_external', true );
		} else if ( $video_type == 'type_youtube') {
			$video_url 		= get_post_meta( $video->ID, 'cytp_video_url', true );
			$videoId 		= cytp_youtube_id_from_url( $video_url );
		}

		$video_width 	= get_post_meta( $video->ID, 'cytp_width', true );
		$video_w_unit 	= get_post_meta( $video->ID, 'cytp_w_unit', true );
		$video_ratio 	= get_post_meta( $video->ID, 'cytp_ratio', true );
		$video_quality 	= get_post_meta( $video->ID, 'cytp_quality', true );
		$video_autoplay = get_post_meta( $video->ID, 'cytp_autoplay', true );

		$video_showcontrol 	= get_post_meta( $video->ID, 'cytp_showcontrol', true );
		$video_enableloop 	= get_post_meta( $video->ID, 'cytp_enableloop', true );
		$video_showinfo 	= get_post_meta( $video->ID, 'cytp_showinfo', true );

		$video_feature 	= get_post_meta( $video->ID, 'cytp_feature', true ); // full | resize

		$video_start 	= get_post_meta( $video->ID, 'cytp_start', true );
		$video_end 		= get_post_meta( $video->ID, 'cytp_end', true );

		$video_resize		= get_post_meta( $video->ID, 'cytp_resize_percent', true );
		$video_contentpos	= get_post_meta( $video->ID, 'cytp_content_position', true );

		$video_showthumb 	= get_post_meta( $video->ID, 'cytp_showthumb', true );
		$video_thumbcorner 	= get_post_meta( $video->ID, 'cytp_thumbcorner', true );

		$video_desc = get_post_meta( $video->ID, 'cytp_lockedcontent', true );
		// $video_desc = apply_filters('the_content', $video_desc);
		$video_desc = str_replace(']]>', ']]&gt;', $video_desc);

		$videoWUnit = ($video_w_unit == 'percent') ? "%" : "px";

		if ( $video_ratio == 's' ) {
			$videoHeightClass = "cytp-ratio-square";
		} else if ( $video_ratio == '3' ) {
			$videoHeightClass = "cytp-ratio-43";
		} else if ( $video_ratio == '9' ) {
			$videoHeightClass = "cytp-ratio-169";
		}

		$output = "<div class='cytp-outer-container {$videoHeightClass}' style='width: {$video_width}{$videoWUnit};' 
			data-vtype='{$video_type}' 
			data-vfeature='{$video_feature}' 
			data-pid='{$video->ID}'
			data-vid='{$videoId}'
			data-cid='{$containerId}' 
			data-start='{$video_start}' 
			data-end='{$video_end}' 
			data-autoplay='{$video_autoplay}' 
			data-showcontrol='{$video_showcontrol}' 
			data-enableloop='{$video_enableloop}' 
			data-showinfo='{$video_showinfo}' 
			data-quality='{$video_quality}' 
			data-resize='{$video_resize}' 
			data-thumb='{$video_showthumb}'>
		<div class='cytp-thumb-container {$video_thumbcorner} cytp-pos-{$video_contentpos}'>
			<div class='cytp-container'><div class='{$videoHeightClass}'><div class='cytp-inner-container'>";

		if ( $video_feature == 'resize' && ( $video_contentpos == 'top' || $video_contentpos == 'left' ) ) {
		    $output .= "<div class='cytp-content'><div class='cytp-vcenter'>" . $video_desc . "</div></div>";
		}

		if ( $video_type == 'type_youtube') {
			$output .= "<div id='cytp-{$containerId}'></div>";
		}
		else {
			$output .= "<div class='cytp-ml-video'>";
			$output .= "<video id='cytp-ml-{$containerId}' width='640' height='390' preload='metadata' controls='controls'>"
						. "<source type='video/mp4' src='{$video_url}' />"
						. "<a href='{$video_url}'>"
							. $video_url
						. "</a>"
					. "</video>";
			$output .= "</div>";
		}

		if ( $video_feature == 'resize' && ( $video_contentpos == 'bottom' || $video_contentpos == 'right' ) ) {
		    $output .= "<div class='cytp-content'><div class='cytp-vcenter'>" . $video_desc . "</div></div>";
		}

		$output .= "</div></div></div></div>
	<div class='cytp-dummy-placeholder {$videoHeightClass}'></div>
</div>";
	}

	return $output;
}

function cytp_locked_content_handler($atts) {
	$a = shortcode_atts( array(
			'name' => '',
	), $atts );
	$postname = $a['name'];

	$video = get_page_by_title( $postname, OBJECT, 'cytp_video');
	$video_desc = get_post_meta( $video->ID, 'cytp_lockedcontent', true );
	$video_desc = apply_filters('the_content', $video_desc);
	$video_desc = str_replace(']]>', ']]&gt;', $video_desc);
	
	$output = "<div class='cytp-locked-content' data-pid='{$video->ID}'>";
		$output .= $video_desc;
	$output .= "</div>";

	return $output;
}

add_shortcode( 'cytp_player', 'cytp_player_handler' );
add_shortcode( 'cytp_locked_content', 'cytp_locked_content_handler' );
// --- End of Shortcode ---

// --- Meta Box ---
function cytp_meta_boxes( $meta_boxes ) {
	$fields = array(
					array(
							'name' 			=> __( 'Video Type', 'cytp_plugin' ),
							'id'			=> "cytp_video_type",
							'type' 			=> 'radio',
							'options' 		=> array(
									'type_media' 	=> __( 'WP Media', 		'cytp_plugin' ),
									'type_external' => __( 'External URL', 	'cytp_plugin' ),
									'type_youtube' 	=> __( 'Youtube Video', 'cytp_plugin' ),
							),
							'std'			=> 'type_media'
					),

					array(
							'name' 			=> __( 'Select Video On WP Media', 'cytp_plugin' ),
							'id'	 		=> "cytp_video_media",
							'type' 			=> 'file_advanced',
							'max_file_uploads'	=> 1,
							'visible' 		=> array(
								'cytp_video_type', '=', 'type_media'
							)
					),
					array(
							'name' 			=> __( 'External Video URL', 'cytp_plugin' ),
							'id'	 		=> "cytp_video_external",
							'type' 			=> 'url',
							'visible' 		=> array(
									'cytp_video_type', '=', 'type_external'
							)
					),
					array(
							'name' 			=> __( 'Youtube Video URL', 'cytp_plugin' ),
							'id'	 		=> "cytp_video_url",
							'type' 			=> 'oembed',
							'desc' 			=> __( 'Only youtube allowed', 'cytp_plugin' ),
							'visible' 		=> array(
									'cytp_video_type', '=', 'type_youtube'
							)
					),

					array(
							'id'	 		=> 'cytp_width',
							'name' 			=> __( 'Width', 'cytp_plugin' ),
							'type' 			=> 'number',
					),
					array(
							'id'			=> 'cytp_w_unit',
							'name'			=> __( 'Width Unit', 'cytp_plugin' ),
							'type'			=> 'radio',
							'options' 		=> array(
									'percent' 	=> __( 'Percent', 'cytp_plugin' ),
									'pixel' 	=> __( 'Pixel', 'cytp_plugin' ),
							),
							'std'			=> 'percent'
					),
					array(
							'id'			=> 'cytp_ratio',
							'name'			=> __( 'Aspect Ratio', 'cytp_plugin' ),
							'type'			=> 'radio',
							'options' 		=> array(
									's' 		=> __( 'Square', 'cytp_plugin' ),
									'3' 		=> __( '4:3', 'cytp_plugin' ),
									'9' 		=> __( '16:9', 'cytp_plugin' ),
							),
							'std'			=> '9'
					),
					array(
							'id'			=> 'cytp_quality',
							'name'			=> __( 'Video Quality', 'cytp_plugin' ),
							'type'			=> 'radio',
							'options' 		=> array(
									'240' 		=> __( 'Small', 'cytp_plugin' ),
									'360' 		=> __( 'Medium', 'cytp_plugin' ),
									'480' 		=> __( 'Large', 'cytp_plugin' ),
									'hd720' 	=> __( 'HD720', 'cytp_plugin' ),
									'hd1080' 	=> __( 'HD1080', 'cytp_plugin' ),
							),
							'std'			=> '360',
							'visible' 		=> array(
									'cytp_video_type', '=', 'type_youtube'
							)
					),
					array(
							'id'		=> 'cytp_autoplay',
							'name'		=> __( 'Auto Play', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'y' 		=> __( 'Yes', 'cytp_plugin' ),
									'n' 		=> __( 'No', 'cytp_plugin' ),
							),
							'std'		=> 'n'
					),
					array(
							'id'		=> 'cytp_showcontrol',
							'name'		=> __( 'Show Control', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'y' 		=> __( 'Yes', 'cytp_plugin' ),
									'n' 		=> __( 'No', 'cytp_plugin' ),
							),
							'std'		=> 'y'
					),
					array(
							'id'		=> 'cytp_enableloop',
							'name'		=> __( 'Enable Loop', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'y' 		=> __( 'Yes', 'cytp_plugin' ),
									'n' 		=> __( 'No', 'cytp_plugin' ),
							),
							'std'		=> 'n'
					),
					array(
							'id'		=> 'cytp_showinfo',
							'name'		=> __( 'Show Information', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'y' 		=> __( 'Yes', 'cytp_plugin' ),
									'n' 		=> __( 'No', 'cytp_plugin' ),
							),
							'std'		=> 'y',
							'hidden' 		=> array(
									'cytp_video_type', '!=', 'type_youtube'
							)
					),

					array(
							'id'		=> 'cytp_feature',
							'name'		=> __( 'Video Feature', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' => array(
									'none' 		=> __( 'None', 'cytp_plugin' ),
									'full' 		=> __( 'Full Screen Video', 'cytp_plugin' ),
									'resize' 	=> __( 'Resize Video with Contents', 'cytp_plugin' ),
									'locked' 	=> __( 'Locked Content in Shortcode', 'cytp_plugin' ),
							),
							'std'		=> 'none'
					),

					array(
							'id'	 	=> 'cytp_start',
							'name' 		=> __( 'Start Featuring In Seconds', 'cytp_plugin' ),
							'type' 		=> 'number',
							'visible' 		=> array(
									'cytp_feature', '!=', 'none'
							)
							
					),
					array(
							'id'	 	=> 'cytp_end',
							'name' 		=> __( 'End Featuring In Seconds', 'cytp_plugin' ),
							'type' 		=> 'number',
							'visible' 		=> array(
									'cytp_feature', '!=', 'none'
							)
					),

					array(
							'id'	 	=> 'cytp_resize_percent',
							'name' 		=> __( 'Resize Percentage', 'cytp_plugin' ),
							'type' 		=> 'number',
							'min'		=> 0,
							'max'		=> 100,
    					    'visible' 		=> array(
    					        'cytp_feature', '=', 'resize'
    					    )
					),
					array(
							'id'	 	=> 'cytp_content_position',
							'name' 		=> __( 'Content Position When Resizing Video', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'top' 		=> __( 'Top', 	'cytp_plugin' ),
									'left' 		=> __( 'Left', 	'cytp_plugin' ),
									'right' 	=> __( 'Right', 'cytp_plugin' ),
									'bottom'	=> __( 'bottom','cytp_plugin' ),
							),
							'visible' 		=> array(
									'cytp_feature', '=', 'resize'
							)
					),

					array(
							'id'		=> 'cytp_showthumb',
							'name'		=> __( 'Show Thumbnail On Scroll', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'y' => __( 'Yes', 'cytp_plugin' ),
									'n' => __( 'No', 'cytp_plugin' ),
							),
							'std'		=> 'n'
					),
					array(
							'id'		=> 'cytp_thumbcorner',
							'name'		=> __( 'Thumbnail Position', 'cytp_plugin' ),
							'type'		=> 'radio',
							'options' 	=> array(
									'bottom-right' 	=> __( 'Bottom Right', 'cytp_plugin' ),
									'top-right' 	=> __( 'Top Right', 'cytp_plugin' ),
									'bottom-left' 	=> __( 'Bottom Left', 'cytp_plugin' ),
									'top-left' 		=> __( 'Top Left', 'cytp_plugin' ),
							),
							'std'		=> 'bottom-right',
							'visible' 	=> array(
									'cytp_showthumb', '=', 'y'
							)
					),
	);

	$fields2 = array (
			array(
					'id'		=> 'cytp_lockedcontent',
					'name'		=> __( 'Locked Content', 'cytp_plugin' ),
					'type'		=> 'wysiwyg',
			)
	);

	$meta_boxes[] = array(
			'id'			=> 'meta-video-options',
			'title'			=> __( 'Video Options', 'cytp_plugin' ),
			'post_types' 	=> 'cytp_video',
			'fields'		=> $fields,
			'validation'	=> array(
					'rules'		=> array(
							'cytp_video_media'	=> array(
									'required'		=> true,
							),
							'cytp_video_external'	=> array(
									'required'		=> true,
							),
							'cytp_video_url'	=> array(
									'required'		=> true,
							),
							'cytp_width'	=> array(
									'required'		=> true,
									'number'		=> true
							),
							'cytp_resize_percent'	=> array(
									'required'		=> true,
									'number'		=> true,
									'max'			=> 100,
									'min'			=> 1
							),
							'cytp_start'	=> array(
									'required'		=> true,
									'number'		=> true
							),
							'cytp_end'		=> array(
									'required'		=> true,
									'number'		=> true
							)
					)
			)
	);
	$meta_boxes[] = array(
			'id'			=> 'meta-locked-content',
			'title'			=> __( 'Locked Content Options', 'cytp_plugin' ),
			'post_types' 	=> 'cytp_video',
			'fields'		 => $fields2
	);

	return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'cytp_meta_boxes' );

function cytp_gen_shortcode_video_options() {
	global $post; $customHtml = '';

	if ( $post && $post->post_status != 'auto-draft' ) {
		$title = $post->post_title;
		$customHtml = '<div class="postbox" style="background: #F0F0F0; color: #000; margin-top: 20px;"><div class="inside">';
		$customHtml .= "[cytp_player name=\"{$title}\"][/cytp_player]";
		$customHtml .= '</div></div>';
	}
	
	echo $customHtml;
}

function cytp_gen_shortcode_locked_content() {
	global $post; $customHtml = '';
	if ( $post && $post->post_status != 'auto-draft' ) {
		$title = $post->post_title;
		$customHtml = '<div class="postbox" style="background: #F0F0F0; color: #000; margin-top: 20px;"><div class="inside">';
		$customHtml .= "[cytp_locked_content name=\"{$title}\"][/cytp_locked_content]";
		$customHtml .= '</div></div>';
	}
	
	echo $customHtml;
}

add_action('rwmb_before_meta-video-options', 'cytp_gen_shortcode_video_options' );
add_action('rwmb_before_meta-locked-content', 'cytp_gen_shortcode_locked_content' );
// --- End of Metabox ---

// --- Logo ---
function cytp_add_plugin_logo() {
	global $my_admin_page;
	$screen = get_current_screen();

	if ( is_admin() ) {
		if ( strpos( $screen->id, 'cytp_video') > -1 ) {
			echo '<img src="' . CYTP_PLUGIN_URL . '/images/logo-300.png" style="margin-top: 10px;"/>';
		}
	}
}
add_action( 'admin_notices', 'cytp_add_plugin_logo' );
// --- End of Logo ---

// --- Helper functions ---
function cytp_youtube_id_from_url($url) {
	$pattern =
	'%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
			;
			$result = preg_match($pattern, $url, $matches);
			if ($result) {
				return $matches[1];
			}
			return false;
}
